﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace http_request
{
    //NotFound
    //BadRequest
    //OK
    //Accepted
    //Created

    class Program
    {
        static void Main(string[] args)
        {
            var http_api_requester = new HttpRequestApi();

            //Console.WriteLine("Add angel code: {0}",
              //                 http_api_requester.AddAnglesAsync(new Content()).Result.StatusCode);

            Console.WriteLine("Update angel code: {0}", 
                               http_api_requester.UpdateAnglesAsync(new Content("d654406d-7f7d-4e01-87e9-50cf7753b155", "новое имя", "PATCH req")).Result.StatusCode);

            Console.WriteLine("Delete angel code: {0}", 
                               http_api_requester.DeleteAnglesAsync("fe88efca-c5a0-4b33-8134-6c7cb3bfa6ed").Result.StatusCode);


            Console.WriteLine("Get angel code: {0}",
                               http_api_requester.GetAnglesAsync("fe88efca-c5a0-4b33-8134-6c7cb3bfa6ed").Result.StatusCode);

            Console.WriteLine("Get all angel code: {0}", 
                               http_api_requester.GetAnglesAsync().Result.StatusCode);
        }
    }
}
