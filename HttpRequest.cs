﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;

namespace http_request
{
    public class HttpRequestApi
    {
        private const string UriRequest = "http://10.7.10.18:5010/api";
        private const string CatalogsRequest = "catalogs";
        private const string AnglesRequest = "angles";

        private readonly HttpClient Client;

        public HttpRequestApi()
        {
            this.Client = new HttpClient();
        }

        public async Task<HttpResponseMessage> UpdateAnglesAsync(Content content)
        {
            return await Client.PatchAsync(Concentrate(UriRequest, CatalogsRequest, AnglesRequest, content.Uuid), 
                                           new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
        }

        public async Task<HttpResponseMessage> GetAnglesAsync(string uuid = null)
        {
            return await Client.GetAsync(Concentrate(UriRequest, CatalogsRequest, AnglesRequest, uuid));
        }

        public async Task<HttpResponseMessage> AddAnglesAsync(Content content)
        {
            return await Client.PostAsync(Concentrate(UriRequest, CatalogsRequest, AnglesRequest), 
                                          new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
        }

        public async Task<HttpResponseMessage> DeleteAnglesAsync(string uuid)
        {
            return await Client.DeleteAsync(Concentrate(UriRequest, CatalogsRequest, AnglesRequest, uuid));
        }

        private static string Concentrate(params string[] urls)
        {
            return string.Join(separator: '/', urls);
        }

    }
}
