using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace http_request
{
    [TestClass]
    public class AnglesTest
    {
        readonly HttpRequestApi Http_requester_api = new HttpRequestApi();

        [TestMethod]
        public async Task TestGetAllAngles()
        {
            HttpResponseMessage result = await Http_requester_api.GetAnglesAsync();

            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public async Task TestGetAngles()
        {
            List<Content> all_content = JsonConvert.DeserializeObject<List<Content>>
                                        (Http_requester_api.GetAnglesAsync().
                                         Result.Content.ReadAsStringAsync().Result);

            HttpResponseMessage result = await Http_requester_api.GetAnglesAsync(all_content[0].Uuid);

            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public async Task TestUpdateAngles()
        {
            List<Content> all_content = JsonConvert.DeserializeObject<List<Content>>
                                        (Http_requester_api.GetAnglesAsync().
                                         Result.Content.ReadAsStringAsync().Result);

            HttpResponseMessage result = await Http_requester_api.UpdateAnglesAsync(all_content[0]);

            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, result.StatusCode);
        }

        [TestMethod]
        public async Task TestAddAngles()
        {
            HttpResponseMessage result = await Http_requester_api.AddAnglesAsync(new Content());

            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Created, result.StatusCode);
        }

        [TestMethod]
        public async Task TestDeleteAngles()
        {
            HttpResponseMessage result = null;
            List<Content> all_content = JsonConvert.DeserializeObject<List<Content>>
                                        (Http_requester_api.GetAnglesAsync().
                                         Result.Content.ReadAsStringAsync().Result);
            foreach (Content content in all_content)
            {
                if (content.Name == "ADDED_test_name")
                {
                    result = await Http_requester_api.DeleteAnglesAsync(content.Uuid);
                    break;
                }
            }
            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, result.StatusCode);
        }
    }
}
