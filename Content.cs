﻿using System;
namespace http_request
{
    public class Content
    {
        public string Uuid;
        public string Name;
        public string SysName;
        public string Comment;
        public string DeviceUuid;
        public Devices[] DevicesAngles = new Devices[0];

        public Content(string name = "ADDED_test_name", 
                       string comment = "test_comment", 
                       string sysName = "sys_name_test", 
                       string deviceUuid = "device_uuid",
                       string uuid = "")
        {
            this.Uuid = uuid.Length == 0 ? Guid.NewGuid().ToString() : uuid;
            this.Name = name;
            this.Comment = comment;
            this.SysName = sysName;
            this.DeviceUuid = deviceUuid;
        }
    }
}
