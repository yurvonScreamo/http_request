﻿using System;
namespace http_request
{
    public class Devices
    {
        public string DeviceUuid;
        public int IpAddressNumber = 312323;
        public string IpAddress = "test_name";
        public string Name = "sys_name_test";
        public string Comment = "test_comment";
        public string BiosRevision = "device";
        public string BoardMfg = "device";
        public string FirmwareRevision = "device";
        public string MacAddress = "device";
        public string ProductName = "device";
        public string ProductPartNumber = "device";
        public string ProductSerial = "device";
        public Devices[] Device = new Devices[0];

        public Devices()
        {
            this.DeviceUuid = Guid.NewGuid().ToString();   
        }
    }
}
